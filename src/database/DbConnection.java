package database;

import java.sql.*;
import java.lang.*;


public class DbConnection {

    private static final String url = "jdbc:postgresql://localhost:5435/db_product";
    private static final String user = "postgres";
    private static final String pw = "Demon";


    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url, user, pw);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
