package database;

import product.Products;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class DbOperation {

    public Statement stmt;
    public ResultSet rs;
    long start,end;
    public PreparedStatement pre;

    public ArrayList<Products> fetchAll(){
        ArrayList<Products> arr = new ArrayList<>();
        start = System.currentTimeMillis();
        try {
            stmt = Objects.requireNonNull(DbConnection.getConnection()).createStatement();
            rs = stmt.executeQuery("SELECT * FROM tb_product");

            while(rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                double price = rs.getDouble(3);
                int qty = rs.getInt(4);
                Date imp = rs.getDate(5);


                Products products = new Products(id, name, price, qty, imp.toLocalDate());
                arr.add(products);
            }
            stmt.close();
            rs.close();
            DbConnection.getConnection().close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        end = System.currentTimeMillis();
        return arr;
    }

    public long getLoadingTime(){
        return end-start;
    }


    public void saveDB(ArrayList<Products> arrayList) throws SQLException {

        try {
            System.out.println("Being Add");
            String sqlInsert = "INSERT INTO tb_product(pro_id, pro_name, pro_price, pro_qty, imp_date) VALUES (?,?,?,?,?)";
            pre = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(sqlInsert);
            //DbConnection.getConnection().setAutoCommit(false);
            for (Products products : arrayList) {
                pre.setInt(1, products.getId());
                pre.setString(2, products.getName());
                pre.setDouble(3, products.getPrice());
                pre.setDouble(4, products.getQty());
                pre.setDate(5, Date.valueOf(products.getImp()));
                pre.addBatch();

            }
                clean();
                pre.executeBatch();
        } catch (Exception e) {
           System.out.println(e.toString());
        } finally {
            pre.close();
            Objects.requireNonNull(DbConnection.getConnection()).close();
        }

    }

    public void clean() throws SQLException {
        String sqlInsert = "DELETE FROM tb_product";
        try (PreparedStatement pre = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(sqlInsert)) {
            pre.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            Objects.requireNonNull(DbConnection.getConnection()).close();
        }
    }



}
