package operation;

import java.util.Scanner;
import java.util.regex.Pattern;

public class GettingInput {

    public Scanner input = new Scanner(System.in);

    /**
     * Returns name by user input after validate
     *
     * @return name value as String
     */
    public String getInputName() {
        String name;
        boolean isName;
        do {
            System.out.print("» Enter Product's Name : ");
                name = input.nextLine();
            isName = Pattern.compile("^[ A-Za-z]+$").matcher(name).matches();
        } while ( !isName);
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    /**
     * Returns name by user input after validate
     *
     * @return name value as String
     */
    public String getYN(String action) {
        String answer;
        boolean isAnswer;
        do {
            System.out.printf("» Are you sure, you want to %s this record? [Y/y] or [N/n] : ",action);
            answer = input.nextLine();
            isAnswer = Pattern.matches("[NnYy]",answer);
        } while ( !isAnswer);
        return answer.toLowerCase();
    }

    /**
     * Returns salary by user input after validate
     *
     * @return salary value as double
     */
    public double getInputPrice() {
        return getDouble("» Enter Product's Price : ");
    }

    /**
     * Returns worked hour by user input after validate
     *
     * @return worked hour value as int
     */
    public int getInputQty() {
        return getInteger("» Enter Product's Quantity : ");
    }
    /**
     * Returns id by user input
     * @return worked hour value as int
     */
    public int getInputId() {
        return getInteger("» Enter Product's ID : ");
    }
    /**
     * Return value if the input is non-negative and integer
     *
     * @param msg String of msg for input
     * @return value as positive integer only
     */
    public int getInteger(String msg) {
        int num = 0;
        boolean isInteger;
        do {
            System.out.print(msg);
            if (input.hasNextInt()) {
                num = input.nextInt();
                isInteger = true;
            } else {
                isInteger = false;
                input.next();
            }
        } while (!isInteger || num < 1);
        input.nextLine();
        return num;
    }

    /**
     * Return value if the input is non-negative and double
     *
     * @param msg String of msg for input
     * @return value as positive double only
     */
    public double getDouble(String msg) {
        double num = 0;
        boolean isDouble;
        do {
            System.out.print(msg);
            if (input.hasNextDouble()) {
                num = input.nextDouble();
                isDouble = true;
            } else {
                isDouble = false;
                input.next();
            }
        } while (!isDouble);
        return num;
    }

    /**
     * Returns value as integer according to minimum and maximun number
     *
     * @param msg String of msg for input
     * @param min minimum input is allowed
     * @param max maximum input is allowed
     *
     * @return value as positive number
     */
    public int getChoice(String msg, int min, int max) {
        int choice = 0;
        boolean isNum;
        do {
            System.out.print(msg);
            if (input.hasNextInt()) {
                choice = input.nextInt();
                isNum = true;
            } else {
                System.out.printf("\n\t\t⚠️ Invalid input!!!! Please Enter only number from %d - %d ⚠️\n", min, max);
                isNum = false;
                input.next();
            }
        } while (!isNum || choice > max);
        return choice;
    }
}
