package operation;

import database.DbOperation;
import file.FileOperation;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import product.Products;
import view.Menus;

import java.io.File;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodOperator{

    /**
     * Instantiate
     */
    public GettingInput gettingInput = new GettingInput();
    public DbOperation dop = new DbOperation();
    public FileOperation fo = new FileOperation();
    Scanner sc = new Scanner(System.in);
    String choose;

    public CellStyle numberStyle = new CellStyle(HorizontalAlign.LEFT);

    //Add product 10M
    public void addTenMRecord(ArrayList<Products> arrList){
        long start = System.currentTimeMillis();
        int id = (arrList.size()!=0?(arrList.get(arrList.size()-1).getId()+1):1);
        for(int i = 0; i < 100; i++){
            arrList.add(new Products(id, "Pepsi", 0.5, 10,  LocalDate.now()));
            id++;
        }
        System.out.println("10 millions record were add successfully. ");
        long end = System.currentTimeMillis();
        System.out.println("This operation took : "+ (end-start));
    }

    public void displayBox(int id, String name, double price, int qty, LocalDate date){
        Table t = new Table(1, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND);
        System.out.println();
        t.setColumnWidth(0, 30, 35);
        t.addCell("ID              :"+id, numberStyle);
        t.addCell("Name            :"+name, numberStyle);
        t.addCell("Unit Price      :"+price, numberStyle);
        t.addCell("Quantity        :"+qty, numberStyle);
        t.addCell("Imported Date   :"+date, numberStyle);
        System.out.println(t.render());
    }


    /**
     * Write new product to List
     * @param arrList list collect as ProductList
     */
    public void write(ArrayList<Products> arrList){
        int id = (arrList.size()!=0)?(arrList.get(arrList.size()-1).getId() +1):1;
        String name;
        double price;
        int qty;
        LocalDate imp = LocalDate.now();

        System.out.println("» Product'ID : "+id);
        name = gettingInput.getInputName();
        price = gettingInput.getInputPrice();
        qty = gettingInput.getInputQty();

        displayBox(id,name,price,qty,imp);
        choose= gettingInput.getYN("add");
        switch (choose.toLowerCase()){
            case "y" :
                Products product = new Products(id,name,price,qty,imp);
                arrList.add(product);
                System.out.println(name+" was add successfully");
                break;
            case "n" :
                System.out.println(name+" didn't add");
                break;
            default:
                System.out.println("Invalid Input");
        }
    }

    /**
     * Display data as table with pagination
     *
     * @param arrList List of data
     * @param page number of page use as start page and current page
     * @param chunk number of rows per page
     */

    public void displayTable(ArrayList<Products> arrList, int page, int chunk){

        int arrSize = arrList.size();
        int totalPage;
        Table t = new Table(5, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.ALL);
        CellStyle cellStyle = new CellStyle(HorizontalAlign.CENTER);

        for(int i=0;i<5;i++)
            t.setColumnWidth(i, 14, 25);

        try {
            if(arrSize!=0){

                //Table header
                t.addCell("Id", cellStyle);
                t.addCell("Name", cellStyle);
                t.addCell("Price", cellStyle);
                t.addCell("Quantity", cellStyle);
                t.addCell("Imported", cellStyle);

                if(arrSize%chunk!=0) {
                    totalPage = (arrSize / chunk)+1; // to make a page for last row which less than 5
                }else{
                    totalPage = arrSize/chunk;
                }

                int tmpRow = (page*chunk)-chunk;
                int length = (page!=totalPage) ? page*chunk : arrSize;

                //Table body
                for(int row = tmpRow; row<length; row++){
                    t.addCell(String.valueOf(arrList.get(row).getId()), cellStyle);
                    t.addCell(arrList.get(row).getName(), cellStyle);
                    t.addCell(String.valueOf(arrList.get(row).getPrice()), cellStyle);
                    t.addCell(String.valueOf(arrList.get(row).getQty()), cellStyle);
                    t.addCell(String.valueOf(arrList.get(row).getImp()), cellStyle);
                }

                //Table Footer
                t.addCell("Page: " +page +" / "+totalPage, new CellStyle(HorizontalAlign.CENTER));
                t.addCell("Total records: " +arrSize +"   ", new CellStyle(HorizontalAlign.RIGHT), 4);
                System.out.println(t.render());
            }else {
                System.out.println("Nothing to show");
            }
        }catch (Exception e){
            System.out.println(e.toString());
        }
    }

    /**
     * Help option
     */
    public void helpProcessing(){
        Table t = new Table(1, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND);
        t.setColumnWidth(0, 70, 75);
        t.addCell("1.   Press   * : Display all record of products",numberStyle);
        t.addCell("2.   Press   w : Add new product",numberStyle);
        t.addCell("     Press   w#proname-unitprice-qty : sortcut for add new product",numberStyle);
        t.addCell("3.   Press   r : read Content any content",numberStyle);
        t.addCell("     Press  r#proId : shortcut for read product by Id",numberStyle);
        t.addCell("4.   Press   u : Update Data",numberStyle);
        t.addCell("5.   Press   d : Delete Data",numberStyle);
        t.addCell("     Press  d#proId : shortcut for delete product by Id",numberStyle);
        t.addCell("6.   Press   f : Display first page",numberStyle);
        t.addCell("7.   Press   p : Display previous page",numberStyle);
        t.addCell("8.   Press   n : Display next page",numberStyle);
        t.addCell("9.   Press   l : Display last page",numberStyle);
        t.addCell("10.  Press   s : Search product by name",numberStyle);
        t.addCell("11.  Press   sa : Save record to file",numberStyle);
        t.addCell("12.  Press   ba : Backup Data",numberStyle);
        t.addCell("13.  Press   re : Restore Data",numberStyle);
        t.addCell("14.  Press   h : Help",numberStyle);
        System.out.println(t.render());
    }

    /**
     * Read products list by id
     */
    public void read(ArrayList<Products> arrayList) {
        int id;
        boolean condition=true;
        id = gettingInput.getInputId();
        for (Products store : arrayList) {
            if (store.getId() == id) {
                displayBox(store.getId(),store.getName(),store.getPrice(),store.getQty(),store.getImp());
                condition = false;
                break;
            }
        }
        if(condition)
        {
            System.out.println("<< ID INVALID IN LIST! >>");
        }
    }

    /**
     * search by name
     */
    public void search(ArrayList<Products> arrayList, int chunk) {

        boolean check=true;
        String names = gettingInput.getInputName();
        String valueStore=names.toLowerCase();
        int page = 1;
        int choice ;
        int count = 0;

        ArrayList<Products> tmpArr = new ArrayList<>();

        Pattern pattern = Pattern.compile(".*"+valueStore+".*");
        Table t = new Table(1, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND);
        t.setColumnWidth(0, 74, 95);
        t.addCell("1. First    2. Previous    3. Next    4. Last    5. Goto    0. Exit"
                , new CellStyle(HorizontalAlign.CENTER));

        for(Products values : arrayList)
        {
            Matcher matcher = pattern.matcher(values.getName().toLowerCase());
            if(matcher.matches())
            {
                count++;
                tmpArr.add(new Products(values.getId(),values.getName(),values.getPrice(),values.getQty(),values.getImp()));
                check=false;
            }
        }

        Menus menus = new Menus();
        int totalPage = menus.getTotalPage(tmpArr,chunk);

        // Print search count
        System.out.println("Product found for [" +names+ "] : "+count);

        if(count == 1){
            displayBox(
                    tmpArr.get(0).getId(),
                    tmpArr.get(0).getName(),
                    tmpArr.get(0).getPrice(),
                    tmpArr.get(0).getQty(),
                    tmpArr.get(0).getImp()
            );
        }else {
            do{
                displayTable(tmpArr, page ,chunk);
                System.out.println(t.render());
                choice = gettingInput.getChoice("» Enter Option:", 0, 5);

                switch (choice){
                    case 1:
                        page = 1;
                        break;
                    case 2:
                        if(page>1){
                            page-=1;
                        }else{
                            page = totalPage;
                        }
                        break;
                    case 3:
                        if(page>=1 && page< totalPage){
                            page+=1;
                        }else{
                            page = 1;
                        }
                        break;
                    case 4:
                        page = totalPage;
                        break;
                    case 5:
                        page = gettingInput.getChoice("» Go to page (1 - " + totalPage +") : ",1, totalPage);
                        break;
                }
            }while (choice!=0);
        }


        if(check)
        {
            System.out.println("YOUR VALUE INPUT NOT EXIT");
        }
    }


    /**
     * method for Update all (Name, Qty, Price)
     * @param i : parameter to get argument that handle specific index of arrylist
     * @param arrayList : arralist of Products
     * @param id : id
     */
    public void updateAll(int i, ArrayList<Products> arrayList, int id, LocalDate date){
        String name;
        double price;
        int qty;
        String choose;
        name = gettingInput.getInputName();
        price = gettingInput.getInputPrice();
        qty = gettingInput.getInputQty();
        System.out.print("Are you sure you want to update this record? [Y/y] or [N,n] :");
        choose= sc.next();
        switch (choose.toLowerCase()){
            case "y" :
                arrayList.get(i).setName(name);
                arrayList.get(i).setPrice(price);
                arrayList.get(i).setQty(qty);
                displayBox(id, name,price,qty, date);
                System.out.println("Product <"+name+"> was updated successfully");
                break;
            case "n" :
                System.out.println("Product <"+name+"> didn't Update");
                break;
            default:
                System.out.println("Invalid Input");
        }
    }

    /**
     * Mehtod UpdateSpecific field such as (Update by name, by qty, by price)
     * @param i : get index of list
     * @param arrayList : arraylist of product
     * @param id field id
     * @param name field name
     * @param price field price
     * @param qty field qty
     */
    public void updateSpecificField(int i, ArrayList<Products> arrayList, int id, String name, double price, int qty, LocalDate date){
        String choose;
        System.out.print("Are you sure you want to update this record? [Y/y] or [N,n] :");
        choose= sc.next();
        switch (choose.toLowerCase()){
            case "y" :
                arrayList.get(i).setName(name);
                arrayList.get(i).setQty(qty);
                arrayList.get(i).setPrice(price);
                displayBox(id, name,price,qty, date);
                System.out.println("Product <"+name+"> was updated successfully");
                break;
            case "n" :
                System.out.println("Product <"+name+"> didn't Update");
                break;
            default:
                System.out.println("Invalid Input");
        }
    }


    /**
     * method for update product in list
     * @param arrayList list of product
     */
    public void update(ArrayList<Products> arrayList) {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND);
        t.setColumnWidth(0, 90, 95);
        int sid;
        int choice;
        boolean isExist = false;
        int tempI = 0;
        LocalDate date = LocalDate.now();

        sid = gettingInput.getInputId();
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getId() == sid) {
                isExist = true;
                tempI = i;
            }
        }
        if (isExist) {
            int id = arrayList.get(tempI).getId();
            String name = arrayList.get(tempI).getName();
            double price = arrayList.get(tempI).getPrice();
            int qty = arrayList.get(tempI).getQty();

            displayBox(id, name, price, qty, date);
            System.out.println("What do you want to update?");
            t.addCell("1. All       2. Name       3. Quantity       4. Unit Price       5. Back to Menu"
                    , new CellStyle(HorizontalAlign.CENTER));
            System.out.println(t.render());

                choice = gettingInput.getChoice("Option (1-5) : ", 1, 5);
                switch (choice) {
                    case 1:
                        updateAll(tempI, arrayList, sid,date);
                        break;
                    case 2:
                        String searchName;
                        searchName = gettingInput.getInputName();
                        updateSpecificField(tempI, arrayList, sid, searchName, price, qty,date);
                        break;
                    case 3:
                        int searchQty;
                        searchQty = gettingInput.getInputQty();
                        updateSpecificField(tempI, arrayList, sid, name, price, searchQty,date);
                        break;
                    case 4:
                        double searchPrice;
                        searchPrice = gettingInput.getInputPrice();
                        updateSpecificField(tempI, arrayList, sid, name, searchPrice, qty,date);
                        break;
                    case 5:
                        break;
                }


        } else {
            System.out.println("Id <" + sid + "> not exit");
        }
    }


    /**
     * method for delete value
     */
    public void deleteValue(ArrayList<Products> arrayList){
        int id;
        System.out.println("Please Input ID of Product : ");
        id = gettingInput.getInputId();

        Iterator<Products> catIterator = arrayList.iterator();
        boolean condition = true;
        while (catIterator.hasNext()) {
            Products store=catIterator.next();
            if(store.getId()==id)
            {
                //display as box
                displayBox(store.getId(),store.getName(),store.getPrice(),store.getQty(),store.getImp());
                condition=false;

                choose= gettingInput.getYN("delete");
                switch (choose.toLowerCase()){
                    case "y" :
                        catIterator.remove();
                        System.out.println(id+" was delete successfully");
                        break;
                    case "n" :
                        System.out.println(id+" didn't delete");
                        break;
                    default:
                        System.out.println("Invalid Input");
                }

                break;
            }
        }

        if(condition)
        {
            System.out.println("<< ID INVALID IN LIST! >>");
        }
    }

    /**
     * Method for backup database
     */
    public void backup(){
        ArrayList<Products> backup = new ArrayList<>(dop.fetchAll());
        fo.backup(backup);
    }

    /**
     * Method to restore database
     */
    public void restore(){
        File folder = new File("FileBackup");
        String[] list = folder.list();

        if (list != null) {
            for(int i=0;i<list.length;i++){
                System.out.println((i+1)+ "). "+ list[i]);
            }
            int choice = gettingInput.getChoice("Choose file to backup : ",1,list.length);
            String name = list[choice-1];
            try {
                dop.saveDB(fo.read(name));
                System.out.println("Database has been restored");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }else {
            System.out.println("There is no backup file !!!");
        }

    }

}
