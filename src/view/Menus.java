package view;
import operation.GettingInput;
import operation.MethodOperator;
import database.DbOperation;
import file.FileOperation;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import product.Products;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Menus {

    /**
     * Instantiate
     */

    public DbOperation dbo = new DbOperation();
    public MethodOperator methodOperator = new MethodOperator();
    public GettingInput gi = new GettingInput();
    public FileOperation fo = new FileOperation();
    public int page = 1;
    public int chunk = 5;

    private void menus() {
        CellStyle numberStyle = new CellStyle(HorizontalAlign.CENTER);

        Table t = new Table(9, BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND_HEADER_FOOTER_AND_COLUMNS);
        for(int i=0;i<9;i++)
            t.setColumnWidth(i, 9, 16);
        t.addCell("*)Display", numberStyle);
        t.addCell("W)rite", numberStyle);
        t.addCell("R)ead", numberStyle);
        t.addCell("U)pdate", numberStyle);
        t.addCell("D)elete", numberStyle);
        t.addCell("F)irst", numberStyle);
        t.addCell("P)revious", numberStyle);
        t.addCell("N)ext", numberStyle);
        t.addCell("L)ast", numberStyle);

        t.addCell("S)earch", numberStyle);
        t.addCell("G)o to", numberStyle);
        t.addCell("Se)t row", numberStyle);
        t.addCell("Sa)ve", numberStyle);
        t.addCell("B)ackup", numberStyle);
        t.addCell("Re)store", numberStyle);
        t.addCell("H)elp", numberStyle);
        t.addCell("E)xit", numberStyle,2);
        System.out.println(t.render());
    }
    public void command(ArrayList<Products> arrayList ) {
        final String recovery = "File/Products.txt";
        String choose;
        boolean check=true;
        int totalPage = getTotalPage( arrayList, chunk);
        Scanner sc=new Scanner(System.in);
        while (check)
        {

            menus();
            System.out.print("Command--> ");
            choose=sc.nextLine();
            switch (choose.toLowerCase())
            {
                case "*":
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "w":
                    methodOperator.write(arrayList);
                    fo.write(arrayList,recovery);
                    totalPage = getTotalPage(arrayList,chunk);
                    break;
                case "r":
                    methodOperator.read(arrayList);
                    break;
                case "u":
                    methodOperator.update(arrayList);
                    fo.write(arrayList,recovery);
                    totalPage = getTotalPage(arrayList,chunk);
                    break;
                case "d":
                    methodOperator.deleteValue(arrayList);
                    fo.write(arrayList,recovery);
                    totalPage = getTotalPage(arrayList,chunk);
                    break;
                case "f":
                    page = 1;
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "p":
                    if(page>1){
                        page-=1;
                    }else{
                        page = totalPage;
                    }
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "n":
                    if(page>=1 && page< totalPage){
                        page+=1;
                    }else{
                        page = 1;
                    }
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "l":
                    page = totalPage;
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "s":
                    methodOperator.search(arrayList, chunk);
                    break;
                case "g":
                    page = gi.getChoice("» Go to page (1 - " + totalPage +") : ",1, totalPage);
                    methodOperator.displayTable(arrayList, page , chunk);
                    break;
                case "se":
                    chunk = gi.getInteger("» Set Row per page : ");
                    totalPage = getTotalPage(arrayList,chunk);
                    break;
                case "sa":
                    try {
                        dbo.saveDB(arrayList);
                        fo.clear(recovery);
                        System.out.println("Save Successful");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case "b":
                    methodOperator.backup();
                    break;
                case "re":
                    methodOperator.restore();
                    arrayList.clear();
                    arrayList = dbo.fetchAll();
                    break;
                case "h":
                    methodOperator.helpProcessing();
                    break;
                case "e":
                    check=false;
                    break;
                case "_10m":
                   methodOperator.addTenMRecord(arrayList);
                   fo.write(arrayList,recovery);
                    totalPage = getTotalPage(arrayList,chunk);
                   break;

                default:
                    System.out.println("YOUR INPUT INVALID");
            }
        }

    }
    public int getTotalPage(ArrayList<Products> arrayList, int chunk){
        if(arrayList.size()%chunk!=0) {
            return  (arrayList.size() / chunk)+1; // to make a page for last row which less than 5
        }else{
            return arrayList.size()/chunk;
        }
    }
}
