package view;

import database.DbOperation;
import file.FileOperation;
import operation.GettingInput;
import product.Products;

import java.sql.SQLException;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {

        /*
          Instantiate
         */

        FileOperation fo = new FileOperation();
        DbOperation dop = new DbOperation();
        Menus objMenu = new Menus();
        GettingInput gi = new GettingInput();

        ArrayList<Products> mainArr;
        final String recovery = "File/Products.txt";
        /*
          Main work
         */

        mainArr = dop.fetchAll(); //Fetch database

        demo_header.run(dop.getLoadingTime()); //Loading header

        // Check if there is recovery
        if(fo.read(recovery).size()>0){

            try {
                System.out.println("You missed saving records !!!");
                String choose= gi.getYN("add");
                switch (choose.toLowerCase()){
                    case "y" :
                        long start = System.currentTimeMillis();
                        dop.saveDB(fo.read(recovery));
                        fo.clear(recovery);
                        mainArr.clear(); //prevent duplicate data
                        mainArr = dop.fetchAll();
                        long end = System.currentTimeMillis();
                        System.out.println("This operation took : "+ (end-start));
                        break;
                    case "n" :
                        System.out.println("There isn't any changed");
                        fo.clear(recovery);
                        break;
                    default:
                        System.out.println("Invalid Input");
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
        objMenu.command(mainArr);

    }
}
