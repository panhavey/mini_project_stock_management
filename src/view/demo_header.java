package view;
class Welcome implements Runnable {
    String message;
    Thread thread;

    Welcome(String str) {
        thread = new Thread(this);
        message = str;
    }

    public void run() {
        PrintMsg(message);
    }

    synchronized void PrintMsg(String message) {
        System.out.printf("%-30s %s\n", "", "Welcome to");
        System.out.printf("%-27s %s\n", "", "Stock Management");
        System.out.format("%20s\n",
                "   _____      _           _____                   _____ ____  \n" +
                "  / ____|    (_)         |  __ \\                 / ____|___ \\ \n" +
                " | (___   ___ _ _ __ ___ | |__) |___  __ _ _ __ | |  __  __) |\n" +
                "  \\___ \\ / _ \\ | '_ ` _ \\|  _  // _ \\/ _` | '_ \\| | |_ ||__ < \n" +
                "  ____) |  __/ | | | | | | | \\ \\  __/ (_| | |_) | |__| |___) |\n" +
                " |_____/ \\___|_|_| |_| |_|_|  \\_\\___|\\__,_| .__/ \\_____|____/ \n" +
                "                                          | |                 \n" +
                "                                          |_|                 \n");

        for (int i = 0; i < message.length(); i++) {
            System.out.print(message.charAt(i));
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

public class demo_header {
    public static void run(long time)
    {
        Welcome t1;
        t1 = new Welcome("Please Wait Loading...\n");
        t1.thread.start();
        try {
            t1.thread.join();
        } catch (InterruptedException e) {
            System.out.println("Thread failed");
        }
        System.out.println("Current Time Loading : " + time);
    }
}