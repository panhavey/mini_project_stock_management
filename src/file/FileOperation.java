package file;

import product.Products;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class FileOperation {
    private BufferedWriter bw;

    public void write(ArrayList<Products> arrayList, String filename) {
        try {
            File file = new File(filename);
            bw = new BufferedWriter(new FileWriter(file));
            bw.write("");
            for (Products pro : arrayList) {
                bw.write(pro.toString());
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Products> read(String filename) {

        ArrayList<Products> arrayList = new ArrayList<>();

        File file = new File(filename);
        String str;
        try {
            BufferedReader br = new BufferedReader((new FileReader(file)));

            if (file.length() != 0 && file.exists()) {
                while ((str = br.readLine()) != null) {
                    Products pro = new Products();
                    String[] data;
                    data = str.split(",");

                    pro.setId(Integer.parseInt(data[0]));
                    pro.setName(data[1]);
                    pro.setPrice(Double.parseDouble(data[2]));
                    pro.setQty(Integer.parseInt(data[3]));
                    pro.setImp(LocalDate.parse(data[4]));

                    arrayList.add(pro);
                }
            }
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return arrayList;
    }

    public void backup(ArrayList<Products> arrayList){
        int h = LocalDateTime.now().getHour();
        int m = LocalDateTime.now().getMinute();
        int s = LocalDateTime.now().getSecond();
        String date = LocalDate.now().toString()+"_" + h + m + s;

        String name = "Backup-"+ date+".bak";
        String path = "FileBackup/" + name;
        write(arrayList,path);
        System.out.println("Backup file has been created : "+ name);
    }

    public void clear(String filename) {
        try {
            bw = new BufferedWriter(new FileWriter(filename));
            bw.write("");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
