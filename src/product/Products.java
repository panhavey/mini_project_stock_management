package product;

import java.time.LocalDate;

public class Products {
    private int id;
    private String name;
    private double price;
    private int qty;

    private LocalDate imp;

    public Products(int id, String name, double price, int qty, LocalDate imp) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.imp = imp;
    }

    public Products() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    public LocalDate getImp() {
        return imp;
    }

    public void setImp(LocalDate imp) {
        this.imp = imp;
    }

    @Override
    public String toString() {
        return id + "," + name + "," + price + "," + qty +"," + imp + "\n";
    }
}
